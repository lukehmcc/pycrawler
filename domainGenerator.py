#Import time module
import time

# Define file path
filepath = 'words.txt'

# Define list to write to
listOfWords = []

#Define TLD
tldList = [".com", ".net", ".org", ".gov"]

# Open file, read it line by line, then write to list
with open(filepath) as fp:
    line = fp.readline()
    cnt = 1
    while line:
        # print(line)
        lineNoNewLine = line[0:-1]
        listOfWords.append(lineNoNewLine)
        line = fp.readline()
        cnt += 1

#define list of SLD
sldList = []

#Time it
start = time.time()

for words in listOfWords:
    sldList.append(words)
    sldList.append(words + words)
    sldList.append(words + words + words)
    for words2 in listOfWords:
        sldList.append(words + words2)
        sldList.append(words2 + words)

finalDomains = []
for sld in sldList:
    for tld in tldList:
        finalDomains.append("https://" + sld + tld + "/index.html")

with open('domainList.txt', 'w') as f:
    for domains in finalDomains:
        f.write("%s\n" % domains)

#finish timing of the things
end = time.time()
print(end - start)