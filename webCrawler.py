import requests

# Define file path
filepath = 'domainList.txt'

listOfLinks = ["https://apple.com/index.html", "https://google.com/index.html", "https://youtube.com/index.html"]
# Open file, read it line by line, then write to list
with open(filepath) as fp:
    line = fp.readline()
    cnt = 1
    while line:
        # print(line)
        lineNoNewLine = line[0:-1]
        listOfLinks.append(lineNoNewLine)
        line = fp.readline()
        cnt += 1
        print(cnt)

c = 0 
# Now iterate through all the links and write them down
for url in listOfLinks:
    try:
        r = requests.get(url, allow_redirects=True)
        path = "downloadedStuff/" + url[8:-11]
        open(path, 'wb').write(r.content)
    except:
        print("dead link")
    percent = c / len(listOfLinks) * 100
    rounded = round(percent)
    print(rounded,  "'%' complete")
    open("index", 'w').write(str(c))
    c += 1
